import argparse
from h5py import File
import numpy as np
from pathlib import Path
from collections import defaultdict
import matplotlib.pyplot as plt
import pandas as pd

parser=argparse.ArgumentParser('Script to create 1D ROC curves data.')
parser.add_argument('-i','--input_file',help='input file name.')
parser.add_argument('-o','--output',help='output file name.')
parser.add_argument('-d','--default_taggers',nargs='+',default=None,help='Use default taggers, for specified datasets [emtopo, pflow, offline].')
parser.add_argument('-e','--emtopo',nargs='+',default=None,help='Emtopo taggers to process.')
parser.add_argument('-p','--pflow',nargs='+',default=None,help='PFlow taggers to process.')
parser.add_argument('-f','--offline',nargs='+',default=None,help='Offline taggers to process.')


def safeLogRatio(num,den):
    '''
    safe log computation, to emulate athena implementation
    <add link here>
    '''
    return np.where(
        den==0., 
        15,
        np.where( 
            num==0.,
            -10,
            np.log(num/den)
    ))

def get_hist(jets, edges, label, tagger, fc = 0.018):
    '''
    Get histogram of D_b for *label* jets
    '''
    valid = jets['HadronConeExclTruthLabelID'] == label
    #valid &= jets['n_super_tracks_associated']>=2
    truth_tagged = jets[valid]
    flav = {f:truth_tagged[f'{tagger}_p{f}'] for f in 'cub'}
    numerator = flav['b']
    denominator = (fc * flav['c'] + (1-fc) * flav['u'])
    discrim = safeLogRatio(numerator,denominator)
    #print('max value of discriminant:',max(discrim[discrim<=20]))
    return np.histogram(discrim, edges)[0]

def get_threshold(x,v):
    ''' 
    returns the threshold (array position) correspoding to a given quantile 
    '''
    norm=v.sum()
    i=0
    p=-1
    for elem in v:
        i+=elem/norm
        q=1-i
        p+=1
        if q<=x:
            return p

def calc_rej(i,v):
    r=v.sum()/v[i:-1].sum()
    return r


def plot_histograms(hists,nj_dict,edges):
    label_dict={0:'light-jets',4:'c-jets',5:'b-jets'}
    
    for dset,h_dset in hists.items():
        print(f'#############   {dset}   ##########')
        print('Used jets:', nj_dict[dset])
        for tagger,h_tagger in h_dset.items():
            fig, ax =plt.subplots(1,1)
            for _,h_label in h_tagger.items():
                nj = f' ({nj_dict[dset][f"{_}"]} jets)'
                ax.plot(edges[1:],h_label/sum(h_label),label=label_dict[_]+nj)
                
           # plt.legend()
            ax.set_xlabel(tagger+' discriminant')
            ax.set_ylabel('density')
            ax.set_xlim(-11,15)
            ax.set_yscale('log')
            ax.legend()
            fig.savefig('_'.join([dset,tagger,'Db'])+'.png')


default_taggers= {
        'emtopo':[
            'fastDips',
            'fastGN120230327',
       ],
        'pflow':[
            'dips20211116',
            'DL1d20211216',
            'fastDIPS20211215',
            'GN120220813',
            'GN120230331',
        ],
        'offline':[
            'GN2v00',
            'DL1dv01',
        ]
    }


def run(args):

    # Build the taggers dictionary from input options
    taggers = {}
    if args.default_taggers is not None:
        for k in args.default_taggers:
            try:
                taggers[k] = default_taggers[k]
            except KeyError:
                raise(f"ERROR: {k} is not a default dataset (emtopo, pflow, offline)")
    else:
        if args.emtopo is not None:
            taggers["emtopo"] = args.emtopo
        if args.pflow is not None:
            taggers["pflow"] = args.pflow
        if args.emtopo is not None:
            taggers["offline"] = args.offline

    # Declare some default values    
    hists ={ 
        'emtopo':  defaultdict(lambda: defaultdict(int)),
        'pflow':   defaultdict(lambda: defaultdict(int)),
        'offline': defaultdict(lambda: defaultdict(int)),
    }

    nj_dict={
        'emtopo':  {'0': 0, '4': 0, '5': 0},
        'pflow':   {'0': 0, '4': 0, '5': 0},
        'offline': {'0': 0, '4': 0, '5': 0},
    }

    infar = np.array([np.inf])
    edges = np.concatenate([-infar,np.linspace(-30,30,3000),infar])

    # Loop over datasets and taggers
    # to build histograms of the discriminants
    for dset in taggers.keys():
        with File(args.input_file, 'r') as h5file:
            try:
                jets = h5file[dset]['jets']
            except KeyError:
                print('WARNING NO KEY')
                jets = h5file['jets']
                      
            #jets= jets[jets['actualInteractionsPerCrossing'] >= 50]
            for tagger in taggers[dset]:
                for label in [0, 4, 5]:
                    print(f'building {tagger} D_b histogram, for {label}')
                    hists[dset][tagger][label] += get_hist(jets, edges, label, tagger)
                    nj_dict[dset][f'{label}']=np.sum(jets['HadronConeExclTruthLabelID']==label)


    plot_histograms(hists,nj_dict,edges)

    # Compute rejections for b-jet efficency levels
    quantiles=np.linspace(0.49,1.0,100)
    out_dict={
        'emtopo':{'effs':quantiles,**nj_dict['emtopo']},
        'pflow': {'effs':quantiles,**nj_dict['pflow']},
        'offline': {'effs':quantiles,**nj_dict['offline']},
    }
    flavours={0:'u',4:'c',5:'b'}
    for dset in taggers.keys():
        for tag in taggers[dset]:
            cutpoints=[]
            # Get cutpoints (as array postions) corresponding to quantile q
            for q in quantiles:
                cutpoints.append(get_threshold(q,hists[dset][tag][5]))

            # Compute rejection factors for light- and c-jets
            for label in [0,4]:
                rej_curve=[]
                for c in cutpoints:
                    rej_curve.append(calc_rej(c,hists[dset][tag][label]))
                out_dict[dset][f'{tag}_{flavours[label]}jets_rej']=rej_curve

    # Save to ouputfile
    outfile=Path(args.output)
    outfile.parent.mkdir(parents=True, exist_ok=True)
    outfile.unlink(missing_ok=True)
    
    for dset in taggers.keys():
        out = pd.DataFrame(out_dict[dset])
        out.to_hdf(outfile,dset)

if __name__=="__main__":
    args=parser.parse_args()
    run(args)

