import argparse
import h5py
import json
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import pandas as pd

from mpl_tools import mpl_config, makeATLAStag

parser=argparse.ArgumentParser('Script to create 1D ROC curves data.')
parser.add_argument('-c','--cfg',help='input config file name.')
parser.add_argument('-o','--output',help='output file name.')


def efficiency_error(epsilon,N):
    '''
    reference: 
        https://inspirehep.net/files/57287ac8e45a976ab423f3dd456af694
    '''
    x=epsilon*(1-epsilon)/N
    # nan safety cut
    x=np.where(x<1e-10,0,x)
    return np.sqrt(x)

def rej_err(rej_rate,N_bkg):
    '''
    Compute the binomial errors on rejections
    considering:  rej. = 1/eff.
    '''
    return np.power(rej_rate,2)*efficiency_error(1./rej_rate,N_bkg)


def plot_roc_comparison(
    Files,
    taggers,
    labels,
    figsize = [11.8, 9],
    colors=['red','blue'],
    lw=[4,4],
    ls=['-','-'],
    lalpha=0.83,
    tag1='Internal Simulation',
    tag2="$\\sqrt{s}=13$ TeV,   $\mathit{t\\bar{t}}$  sample",
    yaxisLabel='Ratio',
    ylims=[1,3.31e4],
    xlims=[0.6,1],
    plt_err=False,
    do_c=False,
    output='roc.png',
   ):
    fig, axes= None, None
    fig, axes = plt.subplots(2,1,figsize=figsize, gridspec_kw={'height_ratios': [2.5,1]}, sharex=True)

    df0 = Files[0]
    x0=df0["effs"]

    axes[0].set_ylim(*ylims)
    axes[0].set_xlim(*xlims)

    # PLOT TOP PANEL
    for i,f in enumerate(Files):
        key="effs"
        rej="jets_rej"
        df = f
        x=df[key]
        y=np.array([*df[f"{taggers[i]}_{'c' if do_c else 'u'}{rej}"]])

        kwargs={"lw":lw[i],"label":labels[i], "c":colors[i], "alpha":lalpha}
        if not colors[i]: kwargs.pop("c")
        plt0 = axes[0].plot(x,y,**kwargs)
       # axes[0].plot(x,df[f"{taggers[i]}_c{rej}"],'-.',lw=lw[i],c=colors[i],alpha=0.8)
        if plt_err:
            print('number of light jets used:',f['0'][0])
            var=rej_err(y,f['0'][0])
            err_band=[y-var, y+var]
            axes[0].fill_between(x,*err_band,color=plt0[0].get_color(),alpha=0.2,)
            
    plt.yscale('log')

    axes[0].set_ylabel(f'{"$c$" if do_c else "$light$"}-jets  rejection')
    axes[1].set_xlabel('$b$-jet efficiency')
    leg = axes[0].legend(fontsize=21,framealpha=0)

    # modifying legend
    '''
    for i,l in enumerate(leg.legendHandles): 
        if i>=0:
            l.set_linewidth(16)
            l.set_data([22,30],[5.6,5.6])
            l.set_alpha(lalpha) 

            #_=l.get_data()
            #print(_)
    '''

    axes[0].set_yscale('log')
    axes[0].grid(True)
    axes[0].grid(True,ls=':',axis='y',which='minor');

    # PLOT RATIO PANEL
    ymax=1
    ymin=1
    for i,f in enumerate(Files):
        key="effs"
        rej="jets_rej"
        df = f
        x=df[key]
        y=np.array([*df[f"{taggers[i]}_{'c' if do_c else 'u'}{rej}"]])
        y0=np.array([*df0[f"{taggers[0]}_{'c' if do_c else 'u'}jets_rej"]])
        ratio=y/y0
        
        ymax = max(ymax,max(ratio))
        ymin = min(ymin,min(ratio))
        
        kwargs={"lw":lw[i],"label":labels[i], "c":colors[i], "alpha":lalpha}
        if not colors[i]:  kwargs.pop("c")

        plt1 = axes[1].plot(x,ratio,ls[i],**kwargs)
    
        if plt_err:
            print('number of light jets used:',f['0'][0])
            var=rej_err(y,f['0'][0])
            var0=rej_err(y0,df0['0'][0])
            ratio_var=y/y0*np.sqrt( (var/y)**2 + ((var0/y0)**2 if i>0 else 0))
            err_band=[y/y0-ratio_var, y/y0+ratio_var]
            axes[1].fill_between(x,*err_band,color=plt1[0].get_color(),alpha=0.2)
    
    #axes[1].plot(x,[1 for i in x],c='r',lw=4)
    axes[1].set_ylabel(yaxisLabel)
    axes[1].grid(True)
    axes[1].grid(True,ls=':',axis='y',which='minor');

    axes[1].set_yscale('linear')
    axes[1].set_xlim(*xlims)
    axes[1].set_ylim(ymin*0.5,ymax*1.1)

    axes[0].tick_params(top=True,right=True,which='both')
    axes[1].tick_params(top=True,right=True,which='both')

    makeATLAStag(axes[0],fig,first_tag=tag1,second_tag=tag2,xmin=0.04,ymax=0.88,size=19,size2=16,sf=1.2)
    flav="_c-rej" if do_c else "_u-rej"
    s_out=output.split('.')
    j_out=''.join([*s_out[:-1],flav])+f'.{s_out[-1]}'
    fig.subplots_adjust(hspace=0.05)
    fig.savefig(j_out)
    return fig, axes


if __name__=="__main__":
    
    mpl_config(1.2)
    
    args=parser.parse_args()

    with open(args.cfg,'r') as f:
        cfg = json.load(f) 

    output = args.output
    
    file_names=[]
    dataset=[]
    taggers=[]
    labels=[]
    colors=[]
    ls=[]
    lw=[]
    Files=[]
    for r in cfg["rocs"]:
        Files.append(pd.read_hdf(r["file"],r["dataset"]))
        taggers.append(r["tagger"])
        colors.append(r.get("color",""))
        ls.append(r.get("ls","-"))
        lw.append(r.get("lw",3))
        labels.append(r.get("label",""))
    

    # --- Plot Parameters --- #
    tag1=' Internal Simulation'
    tag2="$\sqrt{s}=13.6$ TeV, $\mathit{t\\bar{t}}$ sample with pileup"
    
    # ------------------------------#
    fig,_=plot_roc_comparison(
        Files,
        taggers,
        labels,
        figsize=cfg.get("size",[11,9]),
        colors=colors,
        ls=ls,
        lw=lw,
        lalpha=cfg.get("lalpha",0.8),
        tag1=tag1,
        tag2=tag2,
        plt_err=True,
        ylims=cfg.get("ylim_u",[1,4e4]),
        output=output,
    )

    fig,_=plot_roc_comparison(
        Files,
        taggers,
        labels,
        figsize=cfg.get("size",[11,9]),
        colors=colors,
        ls=ls,
        lw=lw,
        lalpha=cfg.get("lalpha",0.8),
        tag1=tag1,
        tag2=tag2,
        plt_err=True,
        ylims=cfg.get("ylim_c",[1,4e2]),
        do_c=True,
        output=output,
    )