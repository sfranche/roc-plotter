import matplotlib.pyplot as plt

def mpl_config(sf=1.5):
    #MATPLOTLIB CONFIGS
    plt.rcParams['font.family'] = 'FreeSans'
  
    # AXIS Labels
    plt.rcParams['axes.labelsize']=20*sf
    plt.rcParams['xaxis.labellocation']='right'
    plt.rcParams['yaxis.labellocation']='top'
    plt.rcParams['axes.linewidth'] = 2*sf
    #+Legend
    plt.rcParams['legend.fontsize']=18*sf
    
    # X-Ticks
    plt.rcParams['xtick.top']=True
    plt.rcParams['xtick.labelsize']=18*sf
    plt.rcParams['xtick.direction']='in'
    plt.rcParams['xtick.major.size']=12*sf
    plt.rcParams['xtick.major.width']=1.8*sf
    #-minor ticks
    plt.rcParams['xtick.minor.visible']=True
    plt.rcParams['xtick.minor.width']=1*sf
    plt.rcParams['xtick.minor.size']=6*sf
    
    # Y-Ticks
    plt.rcParams['ytick.labelsize']=18*sf
    plt.rcParams['ytick.direction']='in'
    plt.rcParams['ytick.major.size']=12*sf
    plt.rcParams['ytick.major.width']=1.8*sf
    #-minor ticks
    plt.rcParams['ytick.minor.visible']=True
    plt.rcParams['ytick.minor.width']=1*sf
    plt.rcParams['ytick.minor.size']=6*sf


def makeATLAStag(ax, fig, first_tag="", second_tag="", xmin=0.04, ymax=0.85,size=14,size2=14,sf=1.5):
    line_spacing = 0.8
    box0 = ax.text(
        xmin,
        ymax-0.001,
        "ATLAS",
        fontweight="bold",
        fontstyle="italic",
        verticalalignment="bottom",
        transform=ax.transAxes,
        size=size*sf,
    )
    box0_ext_tr = ax.transAxes.inverted().transform(
        box0.get_window_extent(renderer=fig.canvas.get_renderer())
    )
    box1 = ax.text(
        box0_ext_tr[1][0],
        ymax,
        " ",
        verticalalignment="bottom",
        transform=ax.transAxes,
        #size=size,
    )
    box1_ext_tr = ax.transAxes.inverted().transform(
        box1.get_window_extent(renderer=fig.canvas.get_renderer())
    )
    ax.text(
        box1_ext_tr[1][0],
        ymax,
        first_tag,
        verticalalignment="bottom",
        transform=ax.transAxes,
        size=size*sf,
    )
    ax.text(
        xmin,
        ymax
        - (box0_ext_tr[1][1] - box0_ext_tr[0][1])
        * (line_spacing + len(second_tag.split("\n")))+0.03,
        second_tag,
        verticalalignment="bottom",
        transform=ax.transAxes,
        size=size2*sf,
    )